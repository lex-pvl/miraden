$(document).ready(function() {
	headerCountryCustomSelect();
	headerNedCustomSelect();
	headerTotalCustomSelect();
	headerCatCustomSelect();
	headerShowForm();
	headerAreaCustomSelect();
	headerRoomCustomSelect();
	headerSeaCustomSelect();
	headerTerCustomSelect();
	headerHasCustomSelect();
	jsText();
	up();
	showLang();
	selects();
	searchCountry();
	searchNed();
	searchPrice();
	searchCategory();
	searchMore();
	sliders();
	tabs();
	showPlayer();
	filmsLength();
	addFixedLeftBlock();
	addFixedRightBlock(); 
	animateLines();
	fixedMenu();
	animatedFooter();
	showTotal();
	starActive();
	showHiddenTags();
	statusChange();
	accordeon();
	sectionOpened();
	cardNavigation();
	disabledButtonStepTwo();
	textLength();
	hideMainRight();
	timerOnCard();
});

function headerCountryCustomSelect() {
	var input = $('#country-input');
	input.on('focus', function() {
		input.siblings('.custom-input-title').addClass('active');
		$(this).siblings('.search-variants').slideDown(400);
		$(this).siblings('.item-svg').addClass('active');
	});

	input.on('blur', function() {
		$(this).siblings('.search-variants').slideUp(400);
		$(this).siblings('.item-svg').removeClass('active');
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	}); 

	var radio = input.siblings('.search-variants').find('input');
	radio.on('change', function() {
		$(this).parent().siblings().find('input').prop('checked', false)
		input.val($(this).val());
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	});
}

function headerNedCustomSelect() {
	var input = $('#ned-input');
	input.on('focus', function() {
		input.siblings('.custom-input-title').addClass('active');
		$(this).siblings('.search-variants').slideDown(400);
		$(this).siblings('.item-svg').addClass('active');
	});

	input.on('blur', function() {
		$(this).siblings('.search-variants').slideUp(400);
		$(this).siblings('.item-svg').removeClass('active');
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	}); 

	var radio = input.siblings('.search-variants').find('.ned-input');
	radio.on('change', function() {
		$(this).parent().siblings().find('.ned-input').prop('checked', false)
		input.val($(this).val());
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	});
}

function headerTotalCustomSelect() {
	var input = $('#total-input');
	input.on('focus', function() {
		input.siblings('.custom-input-title').addClass('active');
		$(this).siblings('.search-variants').slideDown(400);
		$(this).siblings('.item-svg').addClass('active');
	});

	input.on('blur', function() {
		$(this).siblings('.search-variants').slideUp(400);
		$(this).siblings('.item-svg').removeClass('active');
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	}); 

	var radio = input.siblings('.search-variants').find('.total-check');
	radio.on('change', function() {
		$(this).parent().siblings().find('.total-check').prop('checked', false)
		input.val($(this).val());
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	});
}

function headerCatCustomSelect(){
	var input = $('#cat-input');
	input.on('focus', function() {
		input.siblings('.custom-input-title').addClass('active');
		$(this).siblings('.category-variants').slideDown(400);
		$(this).siblings('.item-svg').addClass('active');
	});

	input.on('blur', function() {
		$(this).siblings('.category-variants').slideUp(400);
		$(this).siblings('.item-svg').removeClass('active');
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	}); 

	var radio = input.siblings('.category-variants').find('input');
	radio.on('change', function() {
		$(this).parent().siblings().find('input').prop('checked', false)
		input.val($(this).val());
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	});
}

function headerShowForm() {
	$('.short-item').on('click', function() {
		$(this).toggleClass('active');
		$('#hiddenRow').slideToggle(400);
	});
}

function headerAreaCustomSelect() {
	var input = $('#area-input');
	input.on('focus', function() {
		input.siblings('.custom-input-title').addClass('active');
		$(this).siblings('.search-variants').slideDown(400);
		$(this).siblings('.item-svg').addClass('active');
	});

	input.on('blur', function() {
		$(this).siblings('.search-variants').slideUp(400);
		$(this).siblings('.item-svg').removeClass('active');
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	}); 

	var radio = input.siblings('.search-variants').find('.area-input');
	radio.on('change', function() {
		$(this).parent().siblings().find('.area-input').prop('checked', false)
		input.val($(this).val());
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	});
}

function headerRoomCustomSelect(){
	var input = $('#room-input');
	input.on('focus', function() {
		input.siblings('.custom-input-title').addClass('active');
		$(this).siblings('.category-variants').slideDown(400);
		$(this).siblings('.item-svg').addClass('active');
	});

	input.on('blur', function() {
		$(this).siblings('.category-variants').slideUp(400);
		$(this).siblings('.item-svg').removeClass('active');
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	}); 

	var radio = input.siblings('.category-variants').find('input');
	radio.on('change', function() {
		$(this).parent().siblings().find('input').prop('checked', false)
		input.val($(this).val());
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	});
}

function headerSeaCustomSelect(){
	var input = $('#sea-input');
	input.on('focus', function() {
		input.siblings('.custom-input-title').addClass('active');
		$(this).siblings('.search-variants').slideDown(400);
		$(this).siblings('.item-svg').addClass('active');
	});

	input.on('blur', function() {
		$(this).siblings('.search-variants').slideUp(400);
		$(this).siblings('.item-svg').removeClass('active');
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	}); 

	var radio = input.siblings('.search-variants').find('input');
	radio.on('change', function() {
		$(this).parent().siblings().find('input').prop('checked', false)
		input.val($(this).val());
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	});
}

function headerTerCustomSelect(){
	var input = $('#ter-input');
	input.on('focus', function() {
		input.siblings('.custom-input-title').addClass('active');
		$(this).siblings('.category-variants').slideDown(400);
		$(this).siblings('.item-svg').addClass('active');
	});

	input.on('blur', function() {
		$(this).siblings('.category-variants').slideUp(400);
		$(this).siblings('.item-svg').removeClass('active');
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	}); 

	var radio = input.siblings('.category-variants').find('input');
	radio.on('change', function() {
		$(this).parent().siblings().find('input').prop('checked', false)
		input.val($(this).val());
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	});
}

function headerHasCustomSelect() {
	var input = $('#has-input');
	input.on('focus', function() {
		input.siblings('.custom-input-title').addClass('active');
		$(this).siblings('.category-variants').slideDown(400);
		$(this).siblings('.item-svg').addClass('active');
	});

	input.on('blur', function() {
		$(this).siblings('.category-variants').slideUp(400);
		$(this).siblings('.item-svg').removeClass('active');
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	}); 

	var radio = input.siblings('.category-variants').find('input');
	radio.on('change', function() {
		$(this).parent().siblings().find('input').prop('checked', false)
		input.val($(this).val());
		if (input.val().length !== 0) {
			input.siblings('.custom-input-title').addClass('active');
		} else {
			input.siblings('.custom-input-title').removeClass('active');
		}
	});
}


function jsText() {
	var dataFirst = $('.main-caption').data('first');
	var dataSecond = $('.main-caption').data('second');
	var dataThird = $('.main-caption').data('third');

	$('#first').typeIt({
		strings: [dataFirst, `<strong>${dataSecond}</strong>`, dataThird],
		speed: 50,
		autoStart: false
	});
}

function up() {
	var up = $('#arrowUp');

	up.on('click', function() {
		$('html,body').stop().animate({ scrollTop: 0 }, 2000);
	});
}

function showLang() {
	var lang = $('.lang');

	lang.each(function() {
		var langVariants = $(this).find('.lang-variants');
		$(this).on('click', function() {
			langVariants.toggleClass('active');
		});
	});
}


function selects() {
	var newSelect = $('.new-select');
	newSelect.each(function() {
		$(this).on('select2:open', function() {
			$('body').find('.select2-container').addClass('custom-slct');
		});
	});



	var select = $('.select');
	select.each(function() {
		$(this).select2({
			minimumResultsForSearch: Infinity
		});
	}); 

	var sortSelect = $('.sort-select');
	sortSelect.each(function() {
		$(this).select2({
			minimumResultsForSearch: Infinity
		});
	});

	var stepSelect = $('.select-step');

	function formatState(state) {
		if (!state.id) {
			return state.text;
		}

		if (state.id == 0) {
			// return $('<span class="tr-span">' + state.text + ' <span class="state-star">*</span>' + '</span>');
		} else {
			return state.text;
		}
	}

	stepSelect.each(function() {
		$(this).select2({
			minimumResultsForSearch: Infinity,
			templateResult: formatState,
			templateSelection: formatState
		});

		$(this).on("select2:open", function(e){
			$(this).find('option').prop('disabled', false);
			$(this).siblings('.tr-span').addClass('active')
		})
	});
}

function searchCountry() {
	var search = $('.search-country .search-item-input');
	var searchItem = $('.search-country .search-item');
	var searchVariants = $('.search-country .search-variants');

	$('.search-country .search-item').on('click', function(event) {
		searchVariants.slideDown(400);
		search.focus();
		$('.search-country .search-item-span').addClass('selected');
	});

	$(document).on('mousedown', function(event) {
		if (searchItem.has(event.target).length === 0 && event.target !== searchItem && searchVariants.has(event.target).length === 0 && event.target !== searchVariants) {
			$('.search-country .search-variants').slideUp(400);
			$('.search-country .search-item-span').removeClass('selected');
		} else {

		}
		if (search.val() !== '') {
			$('.search-country .search-item-span').addClass('selected');
		}
	});

	var searchInputs = searchVariants.find('input');
	searchInputs.each(function() {
		$(this).on('change', function() {
			$(this).parent().siblings().find('input').prop('checked', false);
			$(this).parents('.search-variants-items').siblings().find('input').prop('checked', false);
			search.val( $(this).val() );
			$('.search-country .search-variants').slideUp(400);
		});
	});

	search.on('input', function() {
		var str = $(this).val().toLowerCase();
		if (str.length <= 1){
			$('.search-country .search-variants label').show();
		} else {
			$('.search-country .search-variants label').each(function(){
				if ($(this).text().toLowerCase().indexOf(str) < 0){
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		}
	});
}

function searchNed() {
	var search = $('.search-ned .search-item-input');
	var searchItem = $('.search-ned .search-item');
	var searchVariants = $('.search-ned .search-variants');

	$('.search-ned .search-item').on('click', function(event) {
		searchVariants.slideDown(400);
		search.focus();
		$('.search-ned .search-item-span').addClass('selected');
	});

	$(document).on('mousedown', function(event) {
		if (searchItem.has(event.target).length === 0 && event.target !== searchItem && searchVariants.has(event.target).length === 0) {
			$('.search-ned .search-variants').slideUp(400);
			$('.search-ned .search-item-span').removeClass('selected');
		}
		if (search.val() !== '') {
			$('.search-ned .search-item-span').addClass('selected');
		}
	});

	var searchInputs = searchVariants.find('.ned-input');
	searchInputs.each(function() {
		$(this).on('change', function() {
			$(this).parent().siblings().find('.ned-input').prop('checked', false)
			search.val($(this).val());
			$('.search-ned .search-variants').slideUp(400);
		});
	});

	search.on('input', function() {
		var str = $(this).val().toLowerCase();
		if (str.length = 0){
			$('.search-ned .search-variants label').show();
		} else {
			$('.search-ned .search-variants label').each(function(){
				if ($(this).text().toLowerCase().indexOf(str) < 0){
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		}
	});
}

function searchPrice() {
	var search = $('.search-price .search-item-input');
	var searchItem = $('.search-price .search-item');
	var searchVariants = $('.search-price .search-variants');

	$('.search-price .search-item').on('click', function(event) {
		searchVariants.slideDown(400);
		search.focus();
		$('.search-price .search-item-span').addClass('selected');
	});

	$(document).on('mousedown', function(event) {
		if (searchItem.has(event.target).length === 0 && event.target !== searchItem && searchVariants.has(event.target).length === 0) {
			$('.search-price .search-variants').slideUp(400);
			$('.search-price .search-item-span').removeClass('selected');
		}
		if (search.val() !== '') {
			$('.search-price .search-item-span').addClass('selected');
		}
	});

	var searchInputs = searchVariants.find('input');
	searchInputs.each(function() {
		$(this).on('change', function() {
			$(this).parent().siblings().find('input').prop('checked', false)
			search.val($(this).val());
			$('.search-price .search-variants').slideUp(400);
		});
	});

	search.on('input', function() {
		var str = $(this).val().toLowerCase();
		if (str.length = 0){
			$('.search-price .search-variants label').show();
		} else {
			$('.search-price .search-variants label').each(function(){
				if ($(this).text().toLowerCase().indexOf(str) < 0){
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		}
	});
}

function searchCategory() {
	var search = $('.search-category .search-item-input');
	var searchItem = $('.search-category .search-item');
	var searchVariants = $('.search-category .search-variants');

	$('.search-category .search-item').on('click', function(event) {
		searchVariants.slideDown(400);
		search.focus();
		$('.search-category .search-item-span').addClass('selected');
	});

	$(document).on('mousedown', function(event) {
		if (searchItem.has(event.target).length === 0 && event.target !== searchItem && searchVariants.has(event.target).length === 0) {
			$('.search-category .search-variants').slideUp(400);
			$('.search-category .search-item-span').removeClass('selected');
		}
		if (search.val() !== '') {
			$('.search-category .search-item-span').addClass('selected');
		}
	});

	var searchInputs = searchVariants.find('input');
	searchInputs.each(function() {
		$(this).on('change', function() {
			$(this).parent().siblings().find('input').prop('checked', false)
			search.val($(this).val());
			$('.search-category .search-variants').slideUp(400);
		});
	});

	search.on('input', function() {
		var str = $(this).val().toLowerCase();
		if (str.length = 0){
			$('.search-category .search-variants label').show();
		} else {
			$('.search-category .search-variants label').each(function(){
				if ($(this).text().toLowerCase().indexOf(str) < 0){
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		}
	});
}

function searchMore() {
	var search = $('.search-more .search-item-input');
	var searchItem = $('.search-more .search-item');
	var searchVariants = $('.search-more .search-variants');

	$('.search-more .search-item').on('click', function(event) {
		searchVariants.slideDown(400);
		search.focus();
		$('.search-more .search-item-span').addClass('selected');
	});

	$(document).on('mousedown', function(event) {
		if (searchItem.has(event.target).length === 0 && event.target !== searchItem && searchVariants.has(event.target).length === 0) {
			$('.search-more .search-variants').slideUp(400);
			$('.search-more .search-item-span').removeClass('selected');
		}
		if (search.val() !== '') {
			$('.search-more .search-item-span').addClass('selected');
		}
	});

	var searchInputs = searchVariants.find('input');
	searchInputs.each(function() {
		$(this).on('change', function() {
			$(this).parent().siblings().find('input').prop('checked', false)
			search.val($(this).val());
			$('.search-more .search-variants').slideUp(400);
		});
	});

	search.on('input', function() {
		var str = $(this).val().toLowerCase();
		if (str.length = 0){
			$('.search-more .search-variants label').show();
		} else {
			$('.search-more .search-variants label').each(function(){
				if ($(this).text().toLowerCase().indexOf(str) < 0){
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		}
	});
}


function sliders() {
	var swiper = new Swiper('.advantage-container', {
		slidesPerView: 3,
		spaceBetween: 0,
		loop: true,
		navigation: {
			nextEl: '.nav-btn-next',
			prevEl: '.nav-btn-prev',
		},
	});

	var lastSlider = new Swiper('.last-container', {
		slidesPerView: 1,
		spaceBetween: 0,
		navigation: {
			nextEl: '.last-btn-next',
			prevEl: '.last-btn-prev',
		},
	});

	var cardSlider = new Swiper('.card-container', {
		slidesPerView: 1,
		spaceBetween: 0,
		navigation: {
			nextEl: '.card-btn-next',
			prevEl: '.card-btn-prev',
		},
	});

	var planSlider = new Swiper('.full__plan-container', {
		slidesPerView: 1,
		spaceBetween: 0,
		navigation: {
			nextEl: '.full-btn-next',
			prevEl: '.full-btn-prev',
		},
	});

	var topSlider = new Swiper('.full__top-container', {
		slidesPerView: 2,
		spaceBetween: 20,
		navigation: {
			nextEl: '.full__top-btn-next',
			prevEl: '.full__top-btn-prev',
		},
		pagination: {
			el: '.full__top-pagination',
			type: 'fraction',
		},
	});

	var docSlider = new Swiper('.documentation-image-container', {
		slidesPerView: 1,
		spaceBetween: 20,
		navigation: {
			nextEl: '.documentation-btn-next',
			prevEl: '.documentation-btn-prev',
		},
	});



}

function tabs() {
	$('ul.header__right').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.right').find('div.main__content').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.full__info-lists').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.full__info').find('div.full__info-content').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.full__tour-lists').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.full__tour').find('div.full__tour-content').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.full__video-lists').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.full__video').find('div.full__video-content').removeClass('active').eq($(this).index()).addClass('active');
	});
}


function showPlayer() {
	var film = $('.film');
	film.each(function() {
		$(this).on('click', function() {
			$('body').width( $(window).width() );
			$('body').addClass('no-scroll');
			$('.video-overlay').addClass('active');
			$('.video-popup').addClass('active');
			$('.video-popup iframe').attr('src', $(this).attr('data-src'));
		});
		$('.video-overlay').on('click', function() {
			$('body').removeClass('no-scroll');
			$('.video-overlay').removeClass('active');
			$('.video-popup').removeClass('active');
			$('.video-popup iframe').attr('src', '');
		});
	});


	var play = $('.right-play');

	play.each(function() {
		$(this).on('click', function() {
			$('body').width( $(window).width() );
			$('body').addClass('no-scroll');
			$('.video-overlay').addClass('active');
			$('.video-popup').addClass('active');
			$('.video-popup iframe').attr('src', $(this).attr('data-src'));
		});
		$('.video-overlay').on('click', function() {
			$('body').removeClass('no-scroll');
			$('.video-overlay').removeClass('active');
			$('.video-popup').removeClass('active');
			$('.video-popup iframe').attr('src', '');
		});
	});
}

function filmsLength() {
	var limit = 6;
	$(".film:nth-child(n + " + (limit + 1) + ")").hide();

	$(".film-more").click(function(e) {
		e.preventDefault();

		if ($(this).siblings(".film:eq(" + limit + ")").is(":hidden")) {
			$(this).siblings(".film:hidden").show(400);
		} else {
			$(this).siblings(".film:nth-child(n + " + (limit + 1) + ")").hide(400);
		}
	});
}

function cardFullTextItems() {
	var limit = 3;
	$(".card-item-text:nth-child(n + " + (limit + 1) + ")").hide();

	$('.more-link').on('click', function(event) {
		event.preventDefault();

		if ($(this).siblings(".card-item-text:eq(" + limit + ")").is(":hidden")) {
			$(this).siblings(".card-item-text:hidden").slideDown(400);
			$(this).text('скрыть');
		} else {
			$(this).siblings(".card-item-text:nth-child(n + " + (limit + 1) + ")").slideUp(400);
			$(this).text('подробнее ...');
		}
	});
}

cardFullTextItems();

function addFixedLeftBlock() {
	$navLeft = $('.header .left');
	$navLeft.css('width', $navLeft.outerWidth());
	$window = $(window);
	// $h = $navLeft.offset().top;
	$window.scroll(function(){
		if ($window.scrollTop() > 0) {
			$navLeft.addClass('fixed');
		} else {
			$navLeft.removeClass('fixed');
		}
	});
}

function addFixedRightBlock() {
	$navRight = $('.header .right');
	$navRight.css('width', $navRight.outerWidth());
	$window = $(window);
	// $h = $navRight.offset().top;
	$window.scroll(function(){
		if ($window.scrollTop() > 0) {
			$navRight.addClass('fixed-right');
		} else {
			$navRight.removeClass('fixed-right');
		}
	});
}


function animateLines() {
	var lines = $('.vertical-line');

	$(window).on('scroll', function() {
		lines.each(function() {

			if ( $(window).scrollTop() > ( $(this).offset().top - 400 ) ) {
				$(this).addClass('active');
			} else {
				$(this).removeClass('active');
			}

		});
	});
}


function fixedMenu() {
	var openMenu = $('.main__left-menu');
	var centeredFooter = $('.footer__centered');
	var toggleMenu = $('.fixed__menu');

	var offsetLeft = $('.wrapper').offset().left;
	openMenu.on('click', function(e) {
		openMenu.toggleClass('active');
		toggleMenu.toggleClass('active');
		$('.fixed__menu').css('left', offsetLeft + 70);
		centeredFooter.toggleClass('active');
	});
}


function animatedFooter() {
	$(window).on('scroll', function() {
		var maxScroll = $(window).scrollTop();

		if ( maxScroll > ( $('body').height() - $('.footer').height() * 2 ) ) {
			$('.main__left-social .social-link').fadeOut();
			// $('#arrowUp').removeClass('main__left-menu').addClass('transform');
			$('.main__left-menu').fadeOut('fast');
			$('.transform').fadeIn('fast');
		} else {
			$('.main__left-social .social-link').fadeIn();
			$('.main__left-menu').fadeIn('fast');
			$('.transform').fadeOut('fast');
		}
	});
}

function showTotal() {
	var price = $('.price');

	price.each(function() {
		$(this).on('click', function () {
			$(this).toggleClass('active');
			$(this).find('.price__info').toggleClass('active');
		});
	});
}


function starActive() {
	var star = $('.card-name svg');
	star.each(function() {
		$(this).on('click', function(event) {
			$(this).addClass('active');
		});
	});
}


function showHiddenTags() {
	var showed = 4;
	$(".card-tag:nth-child(n + " + (showed + 1) + ")").hide();

	$(".more-tag").click(function(e) {
		e.preventDefault();

		if ($(this).siblings(".card-tag:eq(" + showed + ")").is(":hidden")) {
			$(this).siblings(".card-tag:hidden").fadeIn(400);
		} else {
			$(this).siblings(".card-tag:nth-child(n + " + (showed + 1) + ")").fadeOut(400);
		}
	});
}

function statusChange() {
	var statusInput = $('#status');
	var statusVariant = $('.status-variant');

	statusInput.on('click', function () {
		$('.status-variants').slideToggle(400);
		$(this).parent().addClass('active');
	});
	statusVariant.each(function() {
		$(this).on('click', function () {
			$(this).parent().siblings().find('input').prop('checked', false)
			statusInput.val( $(this).val() );
			statusInput.parent().removeClass('active');
		});
	});	

	$(document).on('mousedown', function(event) {
		if (statusInput.has(event.target).length === 0 && event.target !== statusInput && statusVariant.has(event.target).length === 0) {
			$('.status-variants').slideUp(400);
			statusInput.parent().removeClass('active');
		}
	});
}

function accordeon() {
	var blockItem = $('.zahvat__block-item');

	blockItem.each(function() {
		
		$(this).on('click', function () {
			$(this).siblings().removeClass('active');
			$(this).siblings().find('.zahvat__block-desc').slideUp(400);
			$(this).addClass('active');
			$(this).find('.zahvat__block-desc').slideDown(400);
		});
		if ( $(this).hasClass('active') ) {
			$(this).find('.zahvat__block-desc').slideDown(400);
		}
	});

	var planItem = $('.full__plan-item');

	planItem.each(function() {
		$(this).on('click', function () {
			$(this).siblings().removeClass('active');
			$(this).siblings().find('.full__plan-body').slideUp(400);
			$(this).addClass('active');
			$(this).find('.full__plan-body').slideDown(400);
		});
		if ( $(this).hasClass('active') ) {
			$(this).find('.full__plan-body').slideDown(400);
		}
	});
}

function sectionOpened() {
	var section = $('.section-click');

	section.each(function() {
		$(this).on('click', function() {
			$(this).parent().toggleClass('section-closed');
			$(this).nextAll().slideToggle();
		});
	});

	var docHeader = $('.documentation-form-header');

	docHeader.each(function() {
		$(this).on('click', function () {
			$(this).toggleClass('active');
			$(this).next().slideToggle();
		});
	});
}

function cardNavigation() {

	var nav = $('.card-circle');

	nav.each(function() {
		$(this).on('click', function (e) {
			e.preventDefault();
			setTimeout(function(){
				$(this).addClass('active');
				$(this).siblings().removeClass('active');
			}, 1000)

			var thHref = $(this).attr('href');
			var navSection = $('.nav-section');

			navSection.each(function() {
				if ( thHref === $(this).attr('id') ) {
					$('html,body').stop().animate({ scrollTop: $('#' + $(this).attr('id')).offset().top }, 1000);
				}
			});
		});
	});

	$(window).on('scroll', function() {
		var windscroll = $(window).scrollTop();
		if (windscroll > 0) {
			$('.nav-section').each(function(i) {
				if ($(this).position().top <= windscroll ) {
					$('.card-circle.active').removeClass('active');
					$('.card-circle').eq(i).addClass('active');
				}
			});
		}
	}).scroll();

}

function disabledButtonStepTwo() {
	var stepBtn =  $('#step-two'),
	stepInput = $('.terms-submit-input');

	if (!stepInput.is(':checked')) {
		stepBtn.prop('disabled', true);
		stepBtn.addClass('disabled');
	} else {
		stepBtn.prop('disabled', false);
		stepBtn.removeClass('disabled');
	}

	stepInput.on('change', function() {
		stepBtn.prop('disabled', !$(this).is(':checked'));
		if ($(this).is(':checked')) {
			stepBtn.removeClass('disabled');
		} else {
			stepBtn.addClass('disabled');
		}
	});

	stepBtn.on('click', function () {
		window.location.href = $(this).attr('data-src');
	});
}

function textLength() {
	$('.textarea-field').each(function(){
		var thLength = $(this).attr('maxlength');

		$(this).on('input', function(){
			var textLen = $(this).val().length;
			$(this).siblings().find('.symbols-value').text(textLen)
		});
	});
}

function addArea() {
	var addField = $('.add__area'),
	addedForm = $('.added--area'),
	addedInput = $('.added--input-area'),
	addedBtn = $('.added--btn-area'),
	fields = $('.step__add-areas');

	addField.on('click', function() {
		addedForm.addClass('active');
	});

	addedInput.on('change', function() {
		var thValue = $(this).val();

		addedBtn.on('click', function() {
			var template = `
			<div class="step__add-field">
			<input type="text" class="input-field" required>
			<span class="input-title">
			${thValue}
			</span>
			</div>
			`

			if (thValue !== '') {
				// $('.step__add-areas').append(template);
				$('.add__area').before(template);
			}
			
			setTimeout(function(){
				addedForm.removeClass('active')
			}, 1000);

			template = '';
			thValue = '';
			addedInput.val('');
		});

	});
}

addArea();

function addTech() {
	var addField = $('.add__tech'),
	addedForm = $('.added--tech'),
	addedInput = $('.added--input-tech'),
	addedBtn = $('.added--btn-tech'),
	fields = $('.step__add-tech');

	addField.on('click', function() {
		addedForm.addClass('active');
	});

	addedInput.on('change', function() {
		var thValue = $(this).val();

		addedBtn.on('click', function() {
			var template = `
			<div class="step-select mb-10 mr-3n">
			<span class="tr-span">${thValue} 
			</span>
			<select class="select-step">
			<option value="1" disabled="true">111</option>
			<option value="2" disabled="true">222</option>
			<option value="3" disabled="true">333</option>
			</select>
			</div>
			`

			if (thValue !== '') {
				addField.siblings('.step__add-tech').append(template);
			}
			setTimeout(function(){
				addedForm.removeClass('active')
			}, 1000);

			template = '';
			thValue = '';
			addedInput.val('');
		});

	});
}

// addTech();

function hideMainRight() {
	var close = $('.right-close');
	close.on('click', function () {
		$(this).parent().slideUp(400);
	});
}

function timerOnCard() {

	function getTimeRemaining(endtime) {
		var t = Date.parse(endtime) - Date.parse(new Date());
		var minutes = Math.floor((t / 1000 / 60) % 60);
		var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
		var days = Math.floor(t / (1000 * 60 * 60 * 24));
		return {
			'total': t,
			'days': days,
			'hours': hours,
			'minutes': minutes
		};
	}

	function initializeClock(selector, endtime) {
		var clock = document.getElementById(selector);
		var daysSpan = clock.querySelector('.card__time-day');
		var hoursSpan = clock.querySelector('.card__time-hours');
		var minutesSpan = clock.querySelector('.card__time-min');

		function updateClock() {
			var t = getTimeRemaining(endtime);

			daysSpan.innerHTML = t.days;
			hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
			minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);

			if (t.total <= 0) {
				clearInterval(timeinterval);
			}
		}

		updateClock();
		var timeinterval = setInterval(updateClock, 1000);
	}

	var action = document.querySelectorAll('.action');
	for (var i = 0; i < action.length; i++) {
		initializeClock( action[i].getAttribute('id'), action[i].getAttribute('data-deadline'));
	}
}

function uploadFile() {

	$('#files').on('change', function(event) {
		var files = event.target.files;

		$.each(files, function(index, file) {
			
			var reader = new FileReader();

			reader.readAsDataURL(file);

			reader.onload = function(e) {

				var template = ` 
				<div class="swiper-slide upload-slide">
				<img src="${e.target.result}" alt="Upload">
				</div>
				`

				$('#uploaded-files').append(template);
				$('.step__add-loader').slideDown(400);
				var uploadSlider = new Swiper('.step__add-uploads', {
					slidesPerView: 6,
					spaceBetween: 5,
					navigation: {
						nextEl: '.upload-btn-next',
						prevEl: '.upload-btn-prev',
					},
				})
			}

		});
	});
}

uploadFile();

function stepFourUploadFile() {
	$('#step-file').on('change', function(event) {
		var files = event.target.files;

		$.each(files, function(index, file) {
			
			var reader = new FileReader();

			reader.readAsDataURL(file);

			reader.onload = function(e) {
				var template = ` 
				<img src="${e.target.result}" alt="upload">
				`

				$('.step-uploaded-img').html(template);
			}
		});
	});
}

stepFourUploadFile();

function uploadPhoto() {
	$('#photo-file').on('change', function(event) {
		var files = event.target.files;

		$.each(files, function(index, file) {
			
			var reader = new FileReader();

			reader.readAsDataURL(file);

			reader.onload = function(e) {
				var template = ` 
				<img src="${e.target.result}" alt="upload">
				`

				$('.step-uploaded-photo').html(template);
			}
		});
	});
}

uploadPhoto();

function getSymbol() {
	var symbolInput = document.getElementById('getSymbol');
	var first = document.getElementById('first-symbol');

	if (symbolInput !== null) {
		symbolInput.oninput = function() {
			first.innerHTML = symbolInput.value[0] || '?';
		}
	}
}

getSymbol();

var markers = [];

function initMap() {
	var lng = document.querySelector('.map').getAttribute('data-lng'),
	lat = document.querySelector('.map').getAttribute('data-lat'),
	zoom = document.querySelector('.map').getAttribute('data-zoom'),
	title = document.querySelector('.map').getAttribute('data-title'),
	icon = document.querySelector('.map').getAttribute('data-icon'),
	address = document.querySelector('.map').getAttribute('data-address'),
	city = document.querySelector('.map').getAttribute('data-city'),
	phone = document.querySelector('.map').getAttribute('data-phone'),
	pos = {lat: +lat, lng: +lng},
	style = [
	{
		featureType: "all",
		elementType: "labels.text.fill",
		stylers: [{
			saturation: 36
		}, {
			color: "#000000"
		}, {
			lightness: 40
		}]
	}, {
		featureType: "all",
		elementType: "labels.text.stroke",
		stylers: [{
			visibility: "on"
		}, {
			color: "#000000"
		}, {
			lightness: 16
		}]
	}, {
		featureType: "all",
		elementType: "labels.icon",
		stylers: [{
			visibility: "off"
		}]
	}, {
		featureType: "administrative",
		elementType: "geometry.fill",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 20
		}]
	}, {
		featureType: "administrative",
		elementType: "geometry.stroke",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 17
		}, {
			weight: 1.2
		}]
	}, {
		featureType: "landscape",
		elementType: "all",
		stylers: [{
			visibility: "on"
		}]
	}, {
		featureType: "landscape",
		elementType: "geometry",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 20
		}]
	}, {
		featureType: "landscape",
		elementType: "labels.icon",
		stylers: [{
			saturation: "-100"
		}, {
			lightness: "-54"
		}]
	}, {
		featureType: "poi",
		elementType: "all",
		stylers: [{
			visibility: "on"
		}, {
			lightness: "0"
		}]
	}, {
		featureType: "poi",
		elementType: "geometry",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 21
		}]
	}, {
		featureType: "poi",
		elementType: "labels.icon",
		stylers: [{
			saturation: "-89"
		}, {
			lightness: "-55"
		}]
	}, {
		featureType: "road",
		elementType: "labels.icon",
		stylers: [{
			visibility: "off"
		}]
	}, {
		featureType: "road.highway",
		elementType: "geometry.fill",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 17
		}]
	}, {
		featureType: "road.highway",
		elementType: "geometry.stroke",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 29
		}, {
			weight: .2
		}]
	}, {
		featureType: "road.arterial",
		elementType: "geometry",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 18
		}]
	}, {
		featureType: "road.local",
		elementType: "geometry",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 16
		}]
	}, {
		featureType: "transit",
		elementType: "geometry",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 19
		}]
	}, {
		featureType: "transit.station",
		elementType: "labels.icon",
		stylers: [{
			visibility: "on"
		}, {
			saturation: "-100"
		}, {
			lightness: "-51"
		}]
	}, {
		featureType: "water",
		elementType: "geometry",
		stylers: [{
			color: "#000000"
		}, {
			lightness: 17
		}]
	}
	];

	var options = {
		center: pos,
		zoom: +zoom,
		scrollwheel: !1,
		disableDefaultUI: !1,
		zoomControl: !0,
		streetViewControl: !1,
		fullscreenControl: !0,
		styles: style
	}

	var newMap = new google.maps.Map(document.getElementById("map"), options);

	var marker = new google.maps.Marker({
		position: pos,
		map: newMap,
		title: title,
		icon: icon
	});

	var template = ` 
	<div class="map-template">
	<h3 class="map-address">${address}</h3>
	<p class="map-info">
	${city}
	<a href="tel: + '${phone}'" class="map-phone">${phone}</a>
	</p>
	</div>
	`

	var info = new google.maps.InfoWindow({
		content: template
	});

	info.open(newMap, marker);

	marker.addListener('click', function(){
		info.open(newMap, marker);
	});

	$('.map-btn').each(function() {

		$(this).on('click', function () {
			$(this).addClass('active');
			$(this).siblings().removeClass('active');

			var thisService = $(this).attr('data-service');
			removeMarkers();

			var request = {
				location: pos,
				radius: 200,
				query: thisService
			};

			if (thisService !== 'object') {
				var service = new google.maps.places.PlacesService(newMap);
				service.textSearch(request, callback);
			} else {
				info.open(newMap, marker);
			}
		});
	});

	function callback(results, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			for (var i = 0; i < results.length; i++) {
				var marker = new google.maps.Marker({
					position: results[i].geometry.location,
					map: newMap,
					icon: icon
				});	
				markers.push(marker);
			}
		}
	};

	function removeMarkers() 
	{
		for (i = 0; i < markers.length; i++) {
			markers[i].setMap(null);
		}
		markers = [];
	} 

}

